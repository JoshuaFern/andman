# Andman - Android Manager
I am not responsible for bricked devices, dead SD cards, thermonuclear war, or you getting fired because the alarm app failed. Please do some research if you have any concerns about features included in this script BEFORE using it! YOU are choosing to make these modifications, and if you point the finger at me for messing up your device, I will laugh at you.

## Description
Andman is a simple shell script that I made for myself to automate the process of setup and management of my device via ADB. I have made this script publicly available for anyone to use, but I haven't tested it on any devices but my own. The script should be fairly easy to edit and tailor for your specific device and use case.

## Quick Usage Guide
### Device Setup
No particular device setup is required, just make sure adb debugging is enabled in the developer settings and plug in the device to your PC. Trust the connection when prompted.

Andman supports multiple devices, but only one device should be connected at a time.

### Downloading
Click the cloud icon near the top right of the page, select "Download zip".

### Running
This script depends on adb, curl, sed and grep. Make sure it's all installed and working before running.
You can also edit the settings in andman.sh to your liking.

Make script executable: ```chmod +x ./andman.sh```

Run: ```./andman.sh```

Andman is designed to be run autonomously by default, there will be no user interaction required.

## Additonal Management
ADB is limited and the following are beyond the scope of this project, you must manage these manually.

### Devices without root:
* Set Timezone, Language, etc...

### All devices:
* It's up to you to manage system updates, ROMs, and rooting for your device. 
* Setup your lockscreen security PIN. Consider leaving a message on how to contact you to return the device if it gets lost.
* Consider enabling full-disk encryption, especially if you take your device with you out of the home.
* Remember to add applications you wish to use online to the firewall whitelist. 
